import { Component, OnInit, Input } from '@angular/core';
import { IListaCompras } from '../../interfaces/listaCompras.interface';
import { ModalAddComponent } from '../lista/modal-add/modal-add.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListaService } from '../../services/lista.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  public listaCompra: IListaCompras[];
  public nome: string;
  public check;
  public verify;
  private timeoutHandler: any;

  animationState: string;

  constructor(
    public dialog: MatDialog,
    private listaService: ListaService
  ) {
    this.listaCompra = this.listaService.getListaCompras();
    this.checkIfCheckAll();
  }

  displayedColumns: string[] = ['produto', 'check'];

  ngOnInit() {
  }

  checkIfCheckAll() {
    if (this.listaCompra.length > 0) {
      const feito = this.listaCompra.filter((item) => {
        return item.check === true;
      });
      if (feito.length === this.listaCompra.length) {
        this.verify = true;
      } else {
        this.verify = false;
      }
      this.listaService.updateLista(this.listaCompra);
    }
  }

  selectAll($event) {
    if (this.listaCompra.length > 0) {
      this.listaCompra.map(item => {
        item.check = $event.checked;
      });
      this.listaService.updateLista(this.listaCompra);
    }
  }

  checar($event) {
    console.log(this.listaCompra);
    this.checkIfCheckAll();
  }

  addNew() {
    this.openDialog();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalAddComponent, {
      width: '250px',
      data: { nome: this.nome, check: this.check }
    });

    dialogRef.afterClosed().subscribe(result => {
      const add = {
        nome: result.nome,
        check: result.check ? true : false
      };
      this.listaCompra.push(add);
      this.listaService.updateLista(this.listaCompra);
    });
  }

  finalizar() {
    Swal({
      title: 'Atenção!!!',
      text: 'Você tem certeza de que deseja finalizar essa lista? Isso irá apagar o registro dessa lista em seu dispositivo.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim, eu quero',
      cancelButtonText: 'Não, eu desisto'
    }).then((result) => {
      if (result.value) {
        Swal(
          'Finalizado!',
          'Sua lista de compras foi resetada.',
          'success'
        );
        this.listaCompra = [];
        this.listaService.removerLista();
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal(
        //   'Cancelled',
        //   'Your imaginary file is safe :)',
        //   'error'
        // );
      }
    });
  }

  drop(produto, index) {
    Swal({
      title: 'Atenção!!!',
      text: 'Você tem certeza de que deseja remover ' + produto.nome + ' da sua lista de compras?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim, eu quero',
      cancelButtonText: 'Não, eu desisto'
    }).then((result) => {
      if (result.value) {
        this.listaCompra.splice(index, 1);
        this.listaService.updateLista(this.listaCompra);
        Swal(
          'Finalizado!',
          'Produto removido.',
          'success'
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal(
        //   'Cancelled',
        //   'Your imaginary file is safe :)',
        //   'error'
        // );
      }
    });
  }
}

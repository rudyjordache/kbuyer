import { Component, OnInit, Pipe } from '@angular/core';
import { IListaCompras } from '../../interfaces/listaCompras.interface';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListaService } from '../../services/lista.service';
import Swal from 'sweetalert2';
import { ModalDetailComponent } from './modalDetail/modalDetail.component';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.scss']
})
export class CompraComponent implements OnInit {

  public listaProdutos: any = [
    {
      product_name: 'Exemplo de produto',
      price: 12.5
    }, {
      product_name: 'Exemplo de produto',
      price: 12.5
    }, {
      product_name: 'Exemplo de produto',
      price: 12.5
    }, {
      product_name: 'Exemplo de produto',
      price: 12.5
    }, {
      product_name: 'Exemplo de produto',
      price: 12.5
    }, {
      product_name: 'Exemplo de produto',
      price: 12.5
    }, {
      product_name: 'Exemplo de produto',
      price: 12.5
    }, {
      product_name: 'Exemplo de produto',
      price: 12.5
    }, {
      product_name: 'Exemplo de produto',
      price: 12.5
    }
  ];

  constructor(
    public dialog: MatDialog,
    private listaService: ListaService
  ) { }

  ngOnInit() {
  }

  somaTotal(lista) {
    let total = 0;
    lista.forEach(element => {
      total += element.price;
    });
    return total;
  }

  getDetails(product) {
    console.log(this.listaProdutos[product]);
    const dialogRef = this.dialog.open(ModalDetailComponent, {
      width: '80%',
      data: { product: product }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

}

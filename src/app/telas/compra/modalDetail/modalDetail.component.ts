import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
const _selector = 'app-modalDetail';
@Component({
  selector: _selector,
  templateUrl: './modalDetail.component.html',
  styleUrls: ['./modalDetail.component.scss']
})
export class ModalDetailComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

}

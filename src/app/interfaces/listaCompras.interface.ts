export interface IListaCompras {
  nome: string;
  check: boolean;
}

export interface IProdutoCompras {
  product: string;
  qtd_type_base: string;
  qtd_base: number;
  price_base: number;
  qtd_type: string;
  qtd: number;
  price: number;
}


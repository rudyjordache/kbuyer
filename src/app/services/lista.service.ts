import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListaService {

  constructor() { }

  getListaCompras() {
    const lista = localStorage.getItem('listaProdutos');
    if (lista) {
      return JSON.parse(lista);
    } else {
      return [];
    }
  }

  updateLista(lista): void {
    const json = JSON.stringify(lista);
    localStorage.setItem('listaProdutos', json);
  }

  removerLista(): void {
    localStorage.removeItem('listaProdutos');
  }


}

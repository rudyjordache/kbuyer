import { MatIconModule, MatButtonModule, MatCheckboxModule, MatToolbarModule, MatInputModule } from '@angular/material';
import { MatCardModule, MatSidenavModule, MatExpansionModule, MatSnackBarModule, MatProgressSpinnerModule } from '@angular/material';
import { MatAutocompleteModule, MatSelectModule, MatMenuModule, MatTabsModule, MatChipsModule, MatListModule } from '@angular/material';
import { MatSlideToggleModule, MatTooltipModule, MatProgressBarModule, MatRadioModule, MatDatepickerModule } from '@angular/material';
import { MatNativeDateModule, MatButtonToggleModule, MatSliderModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    MatIconModule, MatButtonModule, MatCheckboxModule,
    MatToolbarModule, MatDialogModule, MatInputModule,
    MatCardModule, MatSidenavModule, MatExpansionModule,
    MatSnackBarModule, MatProgressSpinnerModule, MatAutocompleteModule,
    MatSelectModule, MatMenuModule, MatTabsModule,
    MatChipsModule, MatListModule, MatDatepickerModule,
    MatSlideToggleModule, MatTooltipModule, MatProgressBarModule,
    MatRadioModule, MatNativeDateModule, MatButtonToggleModule,
    MatSliderModule, MatTableModule, DragDropModule
  ],
  exports: [
    MatIconModule, MatButtonModule, MatCheckboxModule,
    MatToolbarModule, MatDialogModule, MatInputModule,
    MatCardModule, MatSidenavModule, MatExpansionModule,
    MatSnackBarModule, MatProgressSpinnerModule, MatAutocompleteModule,
    MatSelectModule, MatMenuModule, MatTabsModule,
    MatChipsModule, MatListModule, MatDatepickerModule,
    MatSlideToggleModule, MatTooltipModule, MatProgressBarModule,
    MatRadioModule, MatNativeDateModule, MatButtonToggleModule,
    MatSliderModule, MatTableModule, DragDropModule
  ]
})
export class MaterialModule { }

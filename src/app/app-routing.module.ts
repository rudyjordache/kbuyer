import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './telas/home/home.component';
import { CompraComponent } from './telas/compra/compra.component';
import { ListaComponent } from './telas/lista/lista.component';
import { SimularComponent } from './telas/simular/simular.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'compra', component: CompraComponent },
  { path: 'lista', component: ListaComponent },
  { path: 'simular', component: SimularComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

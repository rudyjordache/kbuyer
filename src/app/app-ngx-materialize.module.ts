import { NgModule } from '@angular/core';
import {
  MzSidenavModule,
  MzToastModule,
  MzInputModule,
  MzButtonModule,
  MzSelectModule
} from 'ngx-materialize';

@NgModule({
  imports: [
    MzSidenavModule,
    MzToastModule,
    MzInputModule,
    MzButtonModule,
    MzSelectModule
  ],
  exports: [
    MzSidenavModule,
    MzToastModule,
    MzInputModule,
    MzButtonModule,
    MzSelectModule
  ]
})
export class NgxMaterializeModule { }

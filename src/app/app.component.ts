import { Component, Pipe, Directive } from '@angular/core';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private toastService: MzToastService
  ) {
  }


  showToast() {
    this.toastService.show('Feito com amor, por Rudy Jordache', 4000, 'black');
  }

}


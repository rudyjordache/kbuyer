import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './telas/home/home.component';
import { CompraComponent } from './telas/compra/compra.component';
import { ListaComponent } from './telas/lista/lista.component';
import { SimularComponent } from './telas/simular/simular.component';
import { MaterialModule } from './app-material.module';
import { NgxMaterializeModule } from './app-ngx-materialize.module';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { ModalAddComponent } from './telas/lista/modal-add/modal-add.component';
import { ListaService } from './services/lista.service';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { AngularSweetalertComponent } from './components/angular-sweetalert/angular-sweetalert.component';
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { BRLCurrencyPipe } from './pipe/BRLCurrency.pipe';
import { ModalDetailComponent } from './telas/compra/modalDetail/modalDetail.component';


registerLocaleData(localePt, 'pt-BR');

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    // override hammerjs default configuration
    'pan': { threshold: 5 },
    'swipe': {
      velocity: 0.4,
      threshold: 20,
      direction: 31 // /!\ ugly hack to allow swipe in all direction
    }
  };
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CompraComponent,
    ListaComponent,
    SimularComponent,
    ModalAddComponent,
    ModalDetailComponent,
    AngularSweetalertComponent,
    BRLCurrencyPipe
  ],
  exports: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    NgxMaterializeModule,
    FormsModule,

  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    ListaService,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    },
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }
  ],
  entryComponents: [
    ModalAddComponent,
    ModalDetailComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
